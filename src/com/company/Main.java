package com.company;
import java.util.*;
import java.util.ArrayList;
public class Main {

    public static void main(String[] args) {
        Student[] stu = new Student[10];
        menu(stu);
    }

    public static void menu(Student[] stu){
        System.out.println("\t\t\t1.\tlogin");
        System.out.println("\t\t\t2.\tsign up");
        System.out.println("\t\t\t3.\tquite");
        Scanner in = new Scanner(System.in);
        int input;// = new int;
        input = in.nextInt();
        while(input > 3 || input < 1){
            System.out.println("Invadid !!!");
            input = in.nextInt();
        }
        while(input != 3) {
            if (input == 2) {
                choose(stu);
            }
            else if (input == 1) {
                login(stu);
            }
        }
    }
    public static void choose(Student[] stu){
        System.out.println("\t\t\t1.\tstudent");
        System.out.println("\t\t\t2.\tprofessor");
        System.out.println("\t\t\t3.\tamouzesh");
        System.out.println("\t\t\t4.\tquit");
        Scanner in = new Scanner(System.in);
        int input ;//= new int;
        input = in.nextInt();
        while(input > 4 || input < 1){
            System.out.println("Invadid !!!");
            input = in.nextInt();
        }
        if (input == 1){
            buildStudentAcc(stu);
        }
        else if(input == 4){
            menu(stu);
        }

    }
    public static void login(Student[] stu){
        boolean logged = false;

        Scanner in = new Scanner(System.in);
        int ID;

        String pass;
        int count = Student.getCount();
        while(logged = false) {
            System.out.println("enter ID");
            ID = in.nextInt();
            System.out.println("enter password");
            pass = in.nextLine();
            for (int i = 1; i <=count; i++){
                if (ID == stu[i].getStudentID()) {
                    if (pass.equals(stu[i].getPassword())) {
                        System.out.println("you successfully logged in\n");
                        stuAccess(stu, i);
                        logged = true;
                    }
                }
            }
            System.out.println("id or password is incorrect\n1. try again\n2. quit");
            int order = in.nextInt();
            if(order == 2){
                menu(stu);
            }
        }

    }
    public static void buildStudentAcc(Student[] stu){

        Scanner in = new Scanner(System.in);
        System.out.println("first name");
        String name= in.nextLine();
        System.out.println("last name");
        String lastName= in.nextLine();
        System.out.println("student id");
        int stuID= in.nextInt();
        System.out.println();
        System.out.println("campus");
        String campus = in.nextLine();
        System.out.println("major");
        String major= in.nextLine();
        System.out.println("yearOfAcceptance");
        int yearOfAcceptance= in.nextInt();
        System.out.println();
        System.out.println("choose password");
        String password = in.nextLine();
        int i = Student.getCount();
        stu[i] = new Student(name, lastName, stuID, campus, major, yearOfAcceptance, password);
       stuAccess(stu, i);
    }

    public static void stuAccess(Student[] stu, int i){
        System.out.println("\t\t\t1. choose units\n\t\t\t2. edit info\n\t\t\t3. quite");
        Scanner in = new Scanner(System.in);
        int order = in.nextInt();
        while (order > 3 || order < 1){
            System.out.println("Invalid !!!");
            order = in.nextInt();
        }
        if (order == 1){

        }
        else if (order == 2){
            editInfo(stu, i);
        }
        else if (order == 3){
            menu(stu);
        }

    }
    public static void editInfo(Student[] stu, int i){
        System.out.println("1. first name: "+stu[i].getName()+"\n2. last Name: "+stu[i].getLastName()+
                "\n3. Student ID: "+stu[i].getStudentID()+ "\n4. campus: "+ stu[i].getCampus()+ "\n5. major: "+ stu[i].getMajor()
        +"\n6. yearOfAcceptance: "+ stu[i].getYearOfAcceptance() + "\n7. quite");
        Scanner in = new Scanner(System.in);
        int order = in.nextInt();
        if(order == 1){
            System.out.println("enter new name");
            stu[i].setName(in.nextLine())  ;
        }
        else if (order == 2){
            System.out.println("enter new last name");
            stu[i].setLastName(in.nextLine())  ;
        }
        else if (order == 3) {
            System.out.println("enter student ID");
            stu[i].setStudentID(in.nextInt());
        }
        else if(order == 4){
            System.out.println("enter campus");
            stu[i].setCampus(in.nextLine());
        }
        else if(order == 5){
            System.out.println("enter major");
            stu[i].setMajor(in.nextLine());
        }
        else if(order == 6){
            System.out.println("enter getYearOfAcceptance");
            stu[i].setYearOfAcceptance(in.nextInt());
        }

        else if(order == 7){
            menu(stu);
        }
        System.out.println();
        stuAccess(stu, i);

    }

}
