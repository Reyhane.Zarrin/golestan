package com.company;
public class Student {
    private static int count = 0;
    private String name;
    private String lastName;
    private int studentID;
    private String campus;
    private String major;
    private int yearOfAcceptance;
    private String password;

    public Student(String name, String lastName, int studentID, String campus, String major, int yearOfAcceptance, String password) {
        this.name = name;
        this.lastName = lastName;
        this.studentID = studentID;
        this.campus = campus;
        this.major = major;
        this.yearOfAcceptance = yearOfAcceptance;
        this.password = password;
        count++;
    }

    public static int getCount(){
        return count;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public int getYearOfAcceptance() {
        return yearOfAcceptance;
    }

    public void setYearOfAcceptance(int yearOfAcceptance) {
        this.yearOfAcceptance = yearOfAcceptance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
