package com.company;
public class Professor {
    private String name;
    private String lastName;
    private String campus;
    private String group;

    public Professor(String name, String lastName, String campus, String group) {
        this.name = name;
        this.lastName = lastName;
        this.campus = campus;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
