package com.company;
public class Dars {
    private String OnvanDars;
    private String professor;
    private String campus;
    private int studyID;
    private int numberOfUnits;

    public Dars(String onvanDars, String professor, String campus, int studyID, int numberOfUnits) {
        OnvanDars = onvanDars;
        this.professor = professor;
        this.campus = campus;
        this.studyID = studyID;
        this.numberOfUnits = numberOfUnits;
    }

    public String getOnvanDars() {
        return OnvanDars;
    }

    public void setOnvanDars(String onvanDars) {
        OnvanDars = onvanDars;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public int getStudyID() {
        return studyID;
    }

    public void setStudyID(int studyID) {
        this.studyID = studyID;
    }

    public int getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(int numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }
}
